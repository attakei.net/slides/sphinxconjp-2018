from pathlib import Path

from sphinxcontrib.blockdiag import (
    blockdiag_node,
    html_visit_blockdiag,
    html_depart_blockdiag,
)

from slides_workspace.default.conf import *  # noqa
from slides_workspace.sass import make_with_revealjs


here = Path(__file__).parent

extensions += [  # noqa
    "sphinxcontrib.blockdiag",
]

# sphinxcontrib.blockdiag
blockdiag_fontpath = str(here / "_includes/mplus-1p-regular.ttf")
blockdiag_antialias = True
blockdiag_transparency = True
blockdiag_html_image_format = "SVG"


def setup(app):
    app.add_node(
        blockdiag_node, revealjs=(html_visit_blockdiag, html_depart_blockdiag),
    )
    sass_dir = here / "_sass"
    dest_dir = here / "_static/mytheme"
    make_with_revealjs(sass_dir, dest_dir)
