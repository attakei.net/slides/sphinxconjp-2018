# Slide for SphinxCon JP 2018

SphinxCon JP 2018で発表したスライドのソースです。

## ファイル/フォルダ構成

### index.rst

トップソース

### _includes

ソースなどのフォルダ(直接埋め込まれるなどするもの)

### _sass

スタイルシート用のフォルダ
